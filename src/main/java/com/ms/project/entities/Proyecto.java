package com.ms.project.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "proyecto", schema = "proyecto")
public class Proyecto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	private TipoProyecto tipoProyecto;

	@Column(name = "fecha_inicio")
	private Date fechaInicio;

	@Column(name = "tiempo_estimado")
	private int tiempoEstimado;

	@Column(name = "costo_estimado")
	private int costoEstimado;

	@Column(name = "tiempo_real_trabajo")
	private int tiempoRealTrabajo;

	@Column(name = "costo_real")
	private int costoReal;

	@Column(name = "fecha_fin")
	private Date fechaFin;

	@Column(name = "rut_cliente")
	private String rutCliente;

	@Column(name = "rut_decorador")
	private String rutDecorador;

	@Column(name = "activo")
	private int activo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoProyecto getTipoProyecto() {
		return tipoProyecto;
	}

	public void setTipoProyecto(TipoProyecto tipoProyecto) {
		this.tipoProyecto = tipoProyecto;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public int getTiempoEstimado() {
		return tiempoEstimado;
	}

	public void setTiempoEstimado(int tiempoEstimado) {
		this.tiempoEstimado = tiempoEstimado;
	}

	public int getCostoEstimado() {
		return costoEstimado;
	}

	public void setCostoEstimado(int costoEstimado) {
		this.costoEstimado = costoEstimado;
	}

	public int getTiempoRealTrabajo() {
		return tiempoRealTrabajo;
	}

	public void setTiempoRealTrabajo(int tiempoRealTrabajo) {
		this.tiempoRealTrabajo = tiempoRealTrabajo;
	}

	public int getCostoReal() {
		return costoReal;
	}

	public void setCostoReal(int costoReal) {
		this.costoReal = costoReal;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getRutCliente() {
		return rutCliente;
	}

	public void setRutCliente(String rutCliente) {
		this.rutCliente = rutCliente;
	}

	public String getRutDecorador() {
		return rutDecorador;
	}

	public void setRutDecorador(String rutDecorador) {
		this.rutDecorador = rutDecorador;
	}

	public int getActivo() {
		return activo;
	}

	public void setActivo(int activo) {
		this.activo = activo;
	}

	@Override
	public String toString() {
		return "Proyecto [id=" + id + ", tipoProyecto=" + tipoProyecto + ", fechaInicio=" + fechaInicio
				+ ", tiempoEstimado=" + tiempoEstimado + ", costoEstimado=" + costoEstimado + ", tiempoRealTrabajo="
				+ tiempoRealTrabajo + ", costoReal=" + costoReal + ", fechaFin=" + fechaFin + ", rutCliente="
				+ rutCliente + ", rutDecorador=" + rutDecorador + ", activo=" + activo + "]";
	}

}
