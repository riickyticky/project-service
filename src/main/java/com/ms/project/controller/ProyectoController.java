package com.ms.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ms.project.service.ProyectoService;

@RestController
public class ProyectoController {
	
	@Autowired
	private ProyectoService proyectoService;
	
	@GetMapping(value = "/obtener-proyecto-por-id")
	public Object obtenerProyectoPorId(@RequestParam Integer proyectoId) {
		return this.proyectoService.obtenerProyectoPersonalizadoPorId(proyectoId);
	}
}
