package com.ms.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ms.project.entities.Proyecto;
import com.ms.project.repositories.ProyectoRepository;

@Service
public class ProyectoService {
	
	@Autowired
	private ProyectoRepository proyectoRepo;
	
	public Proyecto obtenerProyectoPersonalizadoPorId(Integer proyectoId) {
		return this.proyectoRepo.getOne(proyectoId);
	}
}
